"use strict";

var _xstream = _interopRequireDefault(require("xstream"));

var _run = require("@cycle/run");

var _dom = require("@cycle/dom");

var _http = require("@cycle/http");

var _sweetalert = require("sweetalert");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var popupSuccess = function popupSuccess(response) {
  return (0, _sweetalert.swal)('URL has been shortened!', "<a href=\"/api/shorturl/".concat(response.shortUrl, "\" \n         target=\"_blank\">http://").concat(location.host, ":/api\n         /shorturl/").concat(response.shortUrl, "</a>"), 'success');
};

var popupError = function popupError(response) {
  return (0, _sweetalert.swal)("Error: ".concat(response.error), 'Your submission was invalid. Please try again with valid http(s) URL later.', 'error');
}; // cusm by @teohhanhui


function main(sources) {
  var click$ = sources.DOM.select('.button').events('click');
  var getShortenedURL$ = click$.map(function () {
    return sources.DOM.select('#urlInput').element().map(function (inputElement) {
      return inputElement.value;
    });
  }).flatten().map(function (url) {
    var urlSearchParams = new URLSearchParams();
    urlSearchParams.append('long-url', url);
    return {
      url: 'http://localhost:3000/api/shorten/new',
      method: 'POST',
      category: 'lurl',
      send: urlSearchParams
    };
  });
  var shortenedURL$ = sources.HTTP.select('lurl').flatten().map(function (response) {
    popupSuccess(response.body);
    return response.body.shortUrl;
  });
  var vdom$ = shortenedURL$.startWith('').map(function (shortUrl) {
    return (0, _dom.div)([(0, _dom.input)('#urlInput'), (0, _dom.button)('button btn btn-primary')]);
  });
  return {
    DOM: vdom$,
    HTTP: getShortenedURL$
  };
}

(0, _run.run)(main, {
  DOM: (0, _dom.makeDOMDriver)('#app'),
  HTTP: (0, _http.makeHTTPDriver)()
});