import xs from 'xstream'
import {run} from '@cycle/run'
import {div, input, p, form, button, makeDOMDriver} from '@cycle/dom'
import {makeHTTPDriver} from '@cycle/http'
import {swal} from 'sweetalert'

let popupSuccess = response => {
    return swal(
        'URL has been shortened!',
        `<a href="/api/shorturl/${response.shortUrl}" 
         target="_blank">http://${location.host}:/api
         /shorturl/${response.shortUrl}</a>`,
        'success'
    )
}

let popupError = response => {
    return swal(
        `Error: ${response.error}`,
        'Your submission was invalid. Please try again with valid http(s) URL later.',
        'error'
    )
}

// cusm by @teohhanhui
function main(sources) {
    const click$ = sources.DOM.select('.button').events('click')
    const getShortenedURL$ = click$
          .map(() => sources.DOM.select('#urlInput').element()
               .map(inputElement => inputElement.value)
              )
          .flatten()
          .map(url => {
              const urlSearchParams = new URLSearchParams()
              urlSearchParams.append('long-url', url)

              return {
                  url: 'http://localhost:3000/api/shorten/new',
                  method: 'POST',
                  category: 'lurl',
                  send: urlSearchParams,
              }
          })

    const shortenedURL$ = sources.HTTP.select('lurl')
          .flatten()
          .map(response => {
              popupSuccess(response.body)

              return response.body.shortUrl
          })

    const vdom$ = shortenedURL$
          .startWith('')
          .map(shortUrl =>
               div([
                   input('#urlInput'),
                   button('button btn btn-primary')
               ]))

    
    return {
        DOM: vdom$,
        HTTP: getShortenedURL$
    }
}

run(main, {
    DOM: makeDOMDriver('#app'),
    HTTP: makeHTTPDriver(),
})
